# Telegram Survey Bot

## Usage 
1. write the token in a file named `bot-token.txt` inside project root. (or as an ENV variable named `BOT_TOKEN`.)
2. write the mongodb connection string in a file named `mongo-token.txt` inside project root. (or as an ENV variable named `MONGO_TOKEN`.)
