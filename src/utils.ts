import { existsSync, readFileSync } from "fs";

const getToken = (): string => {
  const token =
    (existsSync("/run/secrets/bot-token.txt") && readFileSync("/run/secrets/bot-token.txt", "utf8").trim()) ||
    (existsSync("bot-token.txt") && readFileSync("bot-token.txt", "utf8").trim()) ||
    process.env["BOT_TOKEN"];

  if (!token) {
    throw new Error("You have to provide the bot-token from @BotFather via environment variable (BOT_TOKEN)");
  }
  return token;
};

const getMongoConnectionString = (): string => {
  const token =
    (existsSync("/run/secrets/mongo-token.txt") && readFileSync("/run/secrets/mongo-token.txt", "utf8").trim()) ||
    (existsSync("mongo-token.txt") && readFileSync("mongo-token.txt", "utf8").trim()) ||
    process.env["MONGO_TOKEN"];

  if (!token) {
    throw new Error("You have to provide mongo DB connection string via environment variable (MONGO_TOKEN)");
  }
  return token;
};

export { getToken, getMongoConnectionString };
