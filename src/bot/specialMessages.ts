import SURVEY from "../../surveys/aa.js";
import DB from "../db/index.js";
import ASK from "./questionsAsk.js";
import { referralNotifyMessage } from "../constants/messages.js";

type Options = { referralID?: string; bot?: any };

const welcomeUser = async (ctx: any, { referralID, bot }: Options) => {
  let referredBy = undefined;
  if (referralID) {
    const parentUser = await DB.User.findOne({ id: referralID });
    if (!parentUser) {
      console.log("wrong referral ID", { referralID });
    } else {
      referredBy = referralID;
      await DB.User.findOneAndUpdate({ id: referralID }, { referFriends: parentUser.referFriends + 1 });
      bot.telegram.sendMessage(referralID, referralNotifyMessage);
    }
  }

  const user = new DB.User({
    activeSurvey: SURVEY.cid,
    section: "a",
    question: 1,
    ...ctx.message.chat,
    completedSurveys: false,
    completedDemographies: false,
    referFriends: 0,
    referredBy,
  });
  user.save().then(() => console.log("New: User Registered"));
  await ctx.reply(SURVEY.welcomeMessage);
  ASK.question({
    section: user.section,
    question: user.question,
    ctx,
  });
  return user;
};

const inviteMessage = (chatId: string | number) => `
${SURVEY.inviteLinkMessage}

📎 t.me/${SURVEY.username}?start=${chatId}
`;

const sendInviteLink = async (ctx: any, chatId: string | number) => {
  console.log("lets send a link for this guy");
  await ctx.reply(inviteMessage(chatId));
};

const sendResults = async (ctx: any) => {
  console.log("wow you can see results");
  await ctx.reply("Results");
};

const specialMessages = {
  welcomeUser,
  sendInviteLink,
  sendResults,
};

export default specialMessages;
