import { generateUpdateMiddleware } from "telegraf-middleware-console-time";
import { Telegraf } from "telegraf";
import TelegrafSessionLocal from "telegraf-session-local";
import { getToken } from "../utils.js";

const bot = new Telegraf(getToken());
const localSession = new TelegrafSessionLocal({
  database: "persist/sessions.json",
});
bot.use(localSession.middleware());
if (process.env["NODE_ENV"] !== "production") {
  bot.use(generateUpdateMiddleware());
}
bot.catch((error) => {
  console.error("telegraf error occurred", error);
});

export default bot;
