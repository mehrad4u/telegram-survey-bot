import { Markup } from "telegraf";

export const generateKeyboard = (question_key: string, responseFormat: string, options?: { name?: string }) => {
  switch (responseFormat) {
    case "five-scale-1":
      return Markup.inlineKeyboard([
        [Markup.button.callback("کاملا موافقم", `${question_key}-1`)],
        [Markup.button.callback("موافقم", `${question_key}-2`)],
        [Markup.button.callback("نه موافق نه مخالف", `${question_key}-3`)],
        [Markup.button.callback("مخالفم", `${question_key}-4`)],
        [Markup.button.callback("کاملا مخالفم", `${question_key}-5`)],
      ]);

    case "five-scale-2":
      return Markup.inlineKeyboard([
        [Markup.button.callback("کاملا این طور هستم", `${question_key}-1`)],
        [Markup.button.callback("تا حد زیادی این طور هستم", `${question_key}-2`)],
        [Markup.button.callback("در حد متوسط این طور هستم", `${question_key}-3`)],
        [Markup.button.callback("کمی این طور هستم", `${question_key}-4`)],
        [Markup.button.callback("اصلا این طور نیستم", `${question_key}-5`)],
      ]);

    case "name":
      return Markup.inlineKeyboard([
        [Markup.button.callback("درسته", `${question_key}-${options?.name}`)],
        [Markup.button.callback("نه، بگذار تصحیح کنم", `retry-name`)],
      ]);
    case "age":
      return Markup.inlineKeyboard([
        [Markup.button.callback("زیر ۱۸ سال دارم", `${question_key}-17`)],
        [
          Markup.button.callback("۱۸", `${question_key}-18`),
          Markup.button.callback("۱۹", `${question_key}-19`),
          Markup.button.callback("۲۰", `${question_key}-20`),
          Markup.button.callback("۲۱", `${question_key}-21`),
          Markup.button.callback("۲۲", `${question_key}-22`),
        ],
        [
          Markup.button.callback("۲۳", `${question_key}-23`),
          Markup.button.callback("۲۴", `${question_key}-24`),
          Markup.button.callback("۲۵", `${question_key}-25`),
          Markup.button.callback("۲۶", `${question_key}-26`),
          Markup.button.callback("۲۷", `${question_key}-27`),
        ],
        [
          Markup.button.callback("۲۸", `${question_key}-28`),
          Markup.button.callback("۲۹", `${question_key}-29`),
          Markup.button.callback("۳۰", `${question_key}-30`),
          Markup.button.callback("۳۱", `${question_key}-31`),
          Markup.button.callback("۳۲", `${question_key}-32`),
        ],
        [
          Markup.button.callback("۳۳", `${question_key}-33`),
          Markup.button.callback("۳۴", `${question_key}-34`),
          Markup.button.callback("۳۵", `${question_key}-35`),
          Markup.button.callback("۳۶", `${question_key}-36`),
          Markup.button.callback("۳۷", `${question_key}-37`),
        ],
        [
          Markup.button.callback("۳۸+", `${question_key}-38+`),
          Markup.button.callback("۵۰+", `${question_key}-50+`),
          Markup.button.callback("۶۰+", `${question_key}-60+`),
          Markup.button.callback("۷۰+", `${question_key}-70+`),
        ],
      ]);

    case "gender":
      return Markup.inlineKeyboard([
        [Markup.button.callback("زن", `${question_key}-F`), Markup.button.callback("مرد", `${question_key}-M`)],
        [
          Markup.button.callback("دیگر", `${question_key}-3`),
          Markup.button.callback("تمایل ندارم بگویم", `${question_key}-no`),
        ],
      ]);

    default:
      throw Error("REEEEE");
  }
};
