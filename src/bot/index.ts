import cron from "node-cron";
import bot from "./initialize.js";
import SURVEY from "../../surveys/aa.js";
import { COMMANDS } from "../constants/commands.js";
import DB from "../db/index.js";
import ASK from "./questionsAsk.js";
import SPECIAL from "./specialMessages.js";
import saveQuestionFunction from "./questionsSave.js";
import showResultsFunction from "./result.js";
// import { demographicsQuestions, AvailableDemographicQuestions } from '../constants/index.js';

async function start(): Promise<void> {
  // The commands you set here will be shown as /commands like /start or /magic in your telegram client.
  await bot.telegram.setMyCommands(COMMANDS);
  bot.action(/.+/, saveQuestionFunction);
  bot.command("results", showResultsFunction);

  // User coming from a REFERRAL LINK
  bot.hears(/^\/start[ =](.+)$/, async (ctx) => {
    const id = ctx.message.chat.id;
    const referralID = ctx.match[1];
    const existingUser = await DB.User.findOne({ id });
    if (!existingUser && referralID) {
      await SPECIAL.welcomeUser(ctx, { referralID, bot });
    } else {
      //	TODO: send an event
    }
  });

  bot.start(async (ctx) => {
    const id = ctx.message.chat.id;
    if (id < 0) {
      console.log({ id });
      console.log("Bot is getting used in a group");
      return;
    }

    const existingUser = await DB.User.findOne({ id });
    if (!existingUser) {
      await SPECIAL.welcomeUser(ctx, {});
      return;
    }

    if (existingUser?.completedDemographies && existingUser?.referFriends <= 0) {
      // user is done scenario
      SPECIAL.sendInviteLink(ctx, id);
    } else if (existingUser?.completedDemographies && existingUser?.referFriends <= 1) {
      // user is done scenario
      SPECIAL.sendResults(ctx);
    } else {
      ASK.question({
        section: existingUser.section,
        question: existingUser.question,
        ctx,
      });
    }
  });

  bot.on("message", async (ctx) => {
    const id = ctx.chat?.id;
    const user = await DB.User.findOne({ id });
    if (user?.currentDemography === "name") {
      ASK.name(ctx);
    } else {
      // TODO
    }
  });

  cron.schedule("*/30 * * * *", () => {
    bot.telegram.sendMessage(SURVEY.administrator, `آمار ربات در نیم ساعت گذشته`);
  });

  await bot.launch();

  console.log(new Date(), "Bot started as", bot.botInfo?.username);
}

export default start;
