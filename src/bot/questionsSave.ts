import type { Context } from "telegraf";
import type { Update } from "typegram";
import SURVEY from "../../surveys/aa.js";
import DB from "../db/index.js";
import ASK from "./questionsAsk.js";
import SPECIAL from "./specialMessages.js";

const name = async ({
  name,
  responseCriteria,
  ctx,
}: {
  name: string;
  responseCriteria: Record<string, unknown>;
  ctx: Context<Update> & {
    match: RegExpExecArray;
  };
}) => {
  if (!ctx.chat?.id) {
    throw Error("id is not defined");
  }

  await DB.Response.findOneAndUpdate(responseCriteria, { name });
  await DB.User.findOneAndUpdate(
    { id: ctx.chat.id },
    {
      completedSurveys: true,
      section: "demographies",
      currentDemography: "age",
    }
  );
  ASK.question({ question: null, section: "demographies", ctx });
};

const age = async ({
  age,
  responseCriteria,
  ctx,
}: {
  age: string;
  responseCriteria: Record<string, unknown>;
  ctx: Context<Update> & {
    match: RegExpExecArray;
  };
}) => {
  if (!ctx.chat?.id) {
    throw Error("id is not defined");
  }

  await DB.Response.findOneAndUpdate(responseCriteria, { age });
  await DB.User.findOneAndUpdate(
    { id: ctx.chat.id },
    {
      currentDemography: "gender",
    }
  );
  ASK.question({ question: null, section: "demographies", ctx });
};

const gender = async ({
  gender,
  responseCriteria,
  ctx,
}: {
  gender: string;
  responseCriteria: Record<string, unknown>;
  ctx: Context<Update> & {
    match: RegExpExecArray;
  };
}) => {
  if (!ctx.chat?.id) {
    throw Error("id is not defined");
  }

  await DB.Response.findOneAndUpdate(responseCriteria, { gender });
  await DB.User.findOneAndUpdate(
    { id: ctx.chat.id },
    {
      completedSurveys: true,
      completedDemographies: true,
      section: null,
      currentDemography: null,
    }
  );
  SPECIAL.sendInviteLink(ctx, ctx.chat.id);
};

const question = async ({
  params,
  ctx,
  responseCriteria,
  chatID,
}: {
  params: Array<string>;
  ctx: Context<Update> & {
    match: RegExpExecArray;
  };
  responseCriteria: Record<string, unknown>;
  chatID: number;
}) => {
  const section = params[0];
  const question = Number(params[1]);
  const questionID = `${section}${question}`;
  const answer = params[2];
  const user = await DB.User.findOneAndUpdate({ id: chatID }, { question: question + 1 });

  const res = await DB.Response.findOne(responseCriteria);
  if (!res) {
    // first response
    const response = new DB.Response({
      responseID: `${SURVEY.cid}-${chatID}`,
      user: chatID,
      survey: SURVEY.cid,
      questions: [
        {
          question: questionID,
          value: answer,
        },
      ],
    });
    response.save().then(() => console.log("New: Response Saved"));
  } else {
    const isDuplicate = res.questions.find((q: any) => q.question === questionID);
    if (isDuplicate) {
      console.log("Duplicate answer found");
      ctx.answerCbQuery(`شما قبلن به این پرسش پاسخ داده‌اید.`);
      return;
    }
    // not first response
    const newValues = res.questions;
    newValues.push({
      question: questionID,
      value: answer,
    });
    await DB.Response.findOneAndUpdate(responseCriteria, { questions: newValues });
  }

  const currentSection = SURVEY.section.find((x) => x.id === user.section);
  const q = Number(user.question);
  if (!currentSection || !currentSection.question[q]) {
    // go to demographies
    await DB.User.findOneAndUpdate(
      { id: chatID },
      {
        completedSurveys: true,
        question: null,
        section: "demographies",
        currentDemography: "name",
      }
    );
    ASK.question({ question: null, section: "demographies", ctx });
  } else {
    ASK.question({
      section: user.section,
      question: q + 1,
      ctx,
    });
    console.log("ask run for q number" + (q + 1));
  }
};

const SAVE = {
  question,
  name,
  age,
  gender,
};

const saveQuestionFunction = async (
  ctx: Context<Update> & {
    match: RegExpExecArray;
  }
): Promise<void> => {
  const chatID = ctx.chat?.id;
  if (typeof chatID !== "number") {
    throw Error("Where is chatID.");
  }
  ctx.deleteMessage();
  const responseCriteria = { responseID: `${SURVEY.cid}-${chatID}` };
  const params: any = ctx.match[0]?.toString()?.split("-");
  switch (params[0]) {
    case "name":
      SAVE.name({ name: params[1], responseCriteria, ctx });
      break;
    case "age":
      SAVE.age({ age: params[1], responseCriteria, ctx });
      break;
    case "gender":
      SAVE.gender({ gender: params[1], responseCriteria, ctx });
      break;
    default:
      SAVE.question({ params, chatID, responseCriteria, ctx });
      break;
  }
};

export default saveQuestionFunction;
