import type { Context } from "telegraf";
import type { Update } from "telegraf/typings/core/types/typegram";
import DB from "../db/index.js";
import SPECIAL from "./specialMessages.js";

const showResultsFunction = async (ctx: Context<Update>) => {
  const id = ctx.chat?.id;
  if (!id) {
    throw Error("is id not defined");
  }

  const user = await DB.User.findOne({ id });
  console.log({ user });
  if (user.referFriends > 0) {
    ctx.reply("نتیجه شما خوب است! ایول");
  } else {
    SPECIAL.sendInviteLink(ctx, id);
  }
};

export default showResultsFunction;
