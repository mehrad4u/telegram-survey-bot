import SURVEY from "../../surveys/aa.js";
import DB from "../db/index.js";
import { generateKeyboard } from "./keyboards.js";
import type { Context } from "telegraf";
import type { Update } from "telegraf/typings/core/types/typegram";

type Param = { section: string; question: number | null; ctx: Context<Update> };
const question = async ({ section, question, ctx }: Param): Promise<void> => {
  // if (user.section === null || user.question === null) {
  // 	throw Error("WTF")
  // }
  const chatID = ctx.chat?.id;

  if (section === "demographies" || question === null) {
    const responseCriteria = { responseID: `${SURVEY.cid}-${chatID}` };
    const res = await DB.Response.findOne(responseCriteria);
    if (!res.name) {
      ctx.reply(`نام شما چیست؟`);
    } else if (!res.age) {
      const keyboard = generateKeyboard("age", "age");
      ctx.reply(`سن شما چقدر است؟`, keyboard);
    } else if (!res.gender) {
      const keyboard = generateKeyboard("gender", "gender");
      ctx.reply(`جنسیت شما چیست؟`, keyboard);
    }
    return;
  }

  const currentSection = SURVEY.section.find((x) => x.id === section);
  if (!currentSection) {
    throw Error("this section is not defined");
  }

  if (!currentSection?.question[question - 1]) {
    if (currentSection?.id === "a") {
      // nest section
      const user = await DB.User.findOneAndUpdate({ id: chatID }, { question: 1, section: "b" });
      console.log("ask run next section" + user.question);
      ASK.question({ question: user.question, section: user.section, ctx });
      return;
    } else if (currentSection?.id === "b") {
      await DB.User.findOneAndUpdate(
        { id: chatID },
        {
          completedSurveys: true,
          question: null,
          section: "demographies",
          currentDemography: "name",
        }
      );
      ASK.question({ question: null, section: "demographies", ctx });
      console.log("go to demography");
    }
  }
  const question_key = `${currentSection.id}-${question}`;
  const keyboard = generateKeyboard(question_key, currentSection.responseFormat);
  if (!currentSection?.question[question - 1]?.title) {
    console.log("REEEEEEEEEEWEEEE");
    return;
  }
  ctx.reply(`${question}. ${currentSection?.question[question - 1]?.title}`, keyboard);
};

const name = (ctx: Context<any>): void => {
  const name: string = ctx.message.text;
  const keys = generateKeyboard("name", "name", { name });
  ctx.reply(`آیا نام شما ${name} است؟`, keys);
};

const ASK = {
  question,
  name,
};

export default ASK;
