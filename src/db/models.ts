import mongoose from "mongoose";
import { getMongoConnectionString } from "../utils.js";
import { ResponseSchema, UserSchema } from "./schema.js";

mongoose.connect(getMongoConnectionString(), { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.set("useFindAndModify", false);
export const User = mongoose.model("User", UserSchema);
export const Response = mongoose.model("Response", ResponseSchema);
