import mongoose from "mongoose";
const { Schema } = mongoose;

export const UserSchema = new Schema(
  {
    id: Number,
    activeSurvey: String,
    section: String,
    question: Number,
    first_name: String,
    last_name: String,
    username: String,
    type: String,
    completedSurveys: Boolean,
    currentDemography: String,
    completedDemographies: Boolean,
    referFriends: Number,
    referredBy: String,
  },
  { timestamps: true }
);

export const ResponseSchema = new Schema(
  {
    responseID: String,
    survey: String,
    user: Number,
    name: String,
    age: Number,
    gender: String,
    questions: [
      {
        question: String,
        value: String,
      },
    ],
  },
  { timestamps: true }
);

export const InviteSchema = new Schema(
  {
    userId: String,
    invitedUserId: String,
  },
  { timestamps: true }
);
