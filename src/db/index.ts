import { User, Response } from "./models.js";

const DB = {
  User,
  Response,
};

export default DB;
