export const referralNotifyMessage = `یک کاربر با کد دعوت شما وارد شد.
برای دریافت نتیجه خود می‌توانید از دستور /results استفاده کنید.`;
