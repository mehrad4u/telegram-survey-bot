/**
 * This const will be used to prepare questions for demography section of survey
 */
const demographicsQuestions = {
  gender: {
    title: "جنسیت شما چیست؟",
    type: "single-answer",
    keys: [
      {
        value: "F",
        title: "زن",
      },
      {
        value: "M",
        title: "مرد",
      },
      {
        value: "C",
        title: "گربه",
      },
    ],
  },
  name: {
    title: "جنسیت شما چیست",
    type: "text",
  },
  age: {
    title: "شما چند سال پربرکت از خدا طلب کردید؟",
    type: "text",
  },
};

export { demographicsQuestions };
export type AvailableDemographicQuestions = keyof typeof demographicsQuestions;
