export const COMMANDS = [
  { command: "start", description: "شروع پرسشنامه" },
  { command: "help", description: "راهنمایی" },
  { command: "results", description: "دریافت نتیجه" },
];
