type SurveyType = {
  name: string;
  username: string;
  welcomeMessage: string;
  inviteLinkMessage: string;
  cid: string;
  administrator: string;
  demographies: Array<"age" | "gender" | "name">;
  section: Array<{
    id: string;
    responseFormat: string;
    question: Array<{
      order: number;
      title: string;
    }>;
  }>;
  responseFormats: Record<string, ResponseFormat>;
};

type ResponseFormat = {
  type: "single-answer";
  keys: Array<{
    value: string;
    title: string;
  }>;
};

export type { SurveyType, ResponseFormat };
